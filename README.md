# Trail Elevation Stats

**Trail Elevation Stats QGIS plugin**

Calculates positive and negative cumulative elevation gain as well as kilometer equivalent (kme) for linestrings.

**Inputs:**
- Elevation raster (DEM)
- Linestring layer containing the trails
- Precision (length of steps used for elevation gain calculation)
- Source of the elevation gain coefficients (see *Kilometer equivalent calculation*)
- Positive elevation gain coefficients (see *Kilometer equivalent calculation*)
- Negative elevation gain coefficients (see *Kilometer equivalent calculation*)
- Elevation gain coefficients per slope class(see *Kilometer equivalent calculation*)


**Kilometer equivalent calculation:**

- If the source of the elevation gain coefficients is *Positive and negative elevation gain coefficients*, kme is calculated as follows:
`kme = [distance in km] + ([cumulative positive elevation gain] / [positive elevation gain coefficient]) + ([cumulative negative elevation gain] / [negative elevation gain coefficient])`

- If the source of the elevation gain coefficients is *Elevation gain coefficient table*, the kme is calculated as the total distance in km plus the sum for each portion of the trail of the elevation gain divided by the coefficient corresponding to the slope of this portion. Trail portion length is defined by the **Precision** field.
    
     

**Outputs:**
 - A copy of the line layer with elevation stats 
 - A point layer which can be used to plot elevation profiles.